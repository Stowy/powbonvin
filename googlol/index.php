<!DOCTYPE html>
<?php
require_once './lib/functions.inc.php';
require_once './lib/google.inc.php';

$word = filter_input(INPUT_GET, "recherche");

$foundSites = array();
if (!empty($word)) {
    $google = new Googlol();
    $foundSites = $google->search($word);
    // var_dump($foundSites);
}
?>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Recherche dico</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.23.0/themes/prism-okaidia.min.css" rel="stylesheet" /> -->
</head>

<body>
    <nav>
        <?php include "./lib/nav.inc.php"; ?>
    </nav>
    <h1>Search word</h1>
    <form action="#" method="get">
        <label for="recherche">Recherchez un mot :</label>
        <input type="text" name="recherche" id="recherche" class="searchBox" value="<?= $word ?>">
        <input type="submit" value="Chercher" class="searchButton">
    </form>
    <h1>Output : </h1>
    <?php
    echo print_articles($foundSites, $word)
    ?>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.23.0/components/prism-core.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.23.0/plugins/autoloader/prism-autoloader.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.23.0/plugins/line-numbers/prism-line-numbers.min.js">
    </script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.23.0/plugins/normalize-whitespace/prism-normalize-whitespace.min.js">
    </script> -->
</body>

</html>