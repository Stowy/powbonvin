<!DOCTYPE html>
<?php
require_once './lib/google.inc.php';

$urlInput = filter_input(INPUT_GET, "urlInput");
$markdown = "";
if (!empty($urlInput)) {
    $google = new Googlol();
    $google->addWebsite($urlInput);
    $markdown = $google->lastMarkdown;
}

?>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Add a site</title>
    <link rel="stylesheet" type="text/css" href="./lib/crud.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.23.0/themes/prism-okaidia.min.css" rel="stylesheet" />
</head>

<body>
    <nav>
        <?php include "./lib/nav.inc.php"; ?>
    </nav>
    <h1>Add a site</h1>
    <form action="#" method="get">
        <label for="urlInput">Entrez une url</label>
        <input type="text" name="urlInput" id="urlInput" class="searchBox" value="<?= $urlInput ?>">
        <input type="submit" value="Add site" class="searchButton">
    </form>
    <h2>Markdown du site</h2>
    <pre>
        <code class="language-markdown">
        <?= $markdown ?>
        </code>
    </pre>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.23.0/components/prism-core.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.23.0/plugins/autoloader/prism-autoloader.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.23.0/plugins/line-numbers/prism-line-numbers.min.js">
    </script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.23.0/plugins/normalize-whitespace/prism-normalize-whitespace.min.js">
    </script>
</body>

</html>