<?php

use League\HTMLToMarkdown\HtmlConverter;

require_once "./lib/constantes.inc.php";

/**
 * Limits the specified string to the specified number of words.
 *
 * @param  string $string
 * @param  int $limit Max number of words in the string.
 * @param  string $end The text to put at the end of the string.
 * @return void
 */
function theme_limit_string($string, $limit = 50, $end = "...")
{
    $string = explode(' ', $string, $limit);
    if (count($string) >= $limit) {
        array_pop($string);
        $string = implode(" ", $string) . $end;
    } else {
        $string = implode(" ", $string);
    }

    return $string;
}

/**
 * Prints the found articles after the search in a pretty way
 *
 * @param  array $foundSites
 * @return void
 */
function print_articles($foundSites, $searchedWord = '')
{
    foreach ($foundSites as $article) {
        $markdown = $article["content_md"];
        $html = $article["content_html"];

        // Clean Markdown
        // $markdown = clean_whitespaces($article["content_md"]);
        $markdown = theme_limit_string($markdown);

        // Clean HTML
        $html = theme_limit_string($html);

        if ($searchedWord != '') {
            // Highlight the searched word
            $markdown = highlight_word($markdown, $searchedWord);
            $html = highlight_word($html, $searchedWord);
        }

        echo "<h2>Article number " . $article["id"] . "</h2>";
        echo "<p>Score : " . $article["score"] . "</p>";
        echo "<p>" . "Url : " . $article["url"] . "</p>";
        echo "<p>Read the : " . $article["created_at"] . "</p>";
        echo '<h3>Markdown</h3>';
        echo '<pre><code class="language-markdown">';
        echo $markdown;
        echo '</code></pre>';
        echo '<h3>HTML</h3>';
        echo '<pre><code class="language-html">';
        echo $html;
        echo '</code></pre>';
    }
}

function clean_whitespaces($text)
{
    $trimmed = trim($text);
    // $no_double_whitespace = preg_replace('/\s{2,}/', "\n", $trimmed);
    $no_double_whitespace = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]{2,}/", "\n", $trimmed);

    return $no_double_whitespace;
}

function highlight_word($text, $word)
{
    $spanned = '<span class="highlighted">' . $word . '</span>';
    $highlighted = str_replace($word, $spanned, $text);

    return $highlighted;
}