<?php
require './vendor/autoload.php';
require_once "./lib/constantes.inc.php";
require_once './lib/functions.inc.php';

use League\HTMLToMarkdown\HtmlConverter;

class Googlol
{
    const TIMEOUT_SEARCH = 5;

    private static $dbh = null;
    private  $pssearch;
    private  $psSaveWebsite;
    public  $from;
    public  $offset;
    public  $lastMarkdown;

    public function __construct()
    {
        $this->from = 0;
        $this->offset = 50;

        if (self::$dbh == null) {
            try {
                self::$dbh = new PDO('mysql:host=' . HOST . ';dbname=' . DBNAME, DBUSER, DBPWD, array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                    PDO::ATTR_PERSISTENT => true,
                ));

                // Prepare search statement
                $sqlsearch = 'SELECT id, content_md, content_html, url, created_at,';
                $sqlsearch .= ' MATCH (content_md) AGAINST (:WORD) as score';
                $sqlsearch .= ' FROM ' . DBNAME . '.' . TABLENAME;
                $sqlsearch .= ' WHERE MATCH (content_md) AGAINST (:WORD)';
                $sqlsearch .= ' ORDER BY score DESC LIMIT :FROM,:OFFSET';
                $this->pssearch = self::$dbh->prepare($sqlsearch);
                $this->pssearch->setFetchMode(PDO::FETCH_ASSOC);

                // Prepare save website statement
                $sqlSaveWebsite = "INSERT INTO " . TABLENAME . " (url, content_md, content_html)";
                $sqlSaveWebsite .= " VALUES (:URL, :MARKDOWN, :HTML);";
                $sqlSaveWebsite .= ' LIMIT :FROM,:OFFSET';
                $this->psSaveWebsite = self::$dbh->prepare($sqlSaveWebsite);
                $this->psSaveWebsite->setFetchMode(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                print "Erreur !: " . $e->getMessage() . "<br>";
                die();
            }
        }
    }

    public function addWebsite(string $url)
    {
        $html = $this->getHtmlFromUrl($url);

        // Clean the html string
        $html = clean_whitespaces($html);

        // Convert to Markdown
        $converter = new HtmlConverter([
            "strip_tags" => true,
            "remove_nodes" => "php a style script footer noscript head nav",
            "hard_break" => true,
            "use_autolinks" => true,
            "header_style" => "atx",
        ]);
        $markdown = $converter->convert($html);

        // Clean markdown and html
        $markdown = clean_whitespaces($markdown);
        $html = htmlspecialchars($html);

        // Save markdown and the site in db
        $this->lastMarkdown = $markdown;
        $this->saveWebsite($url, $html, $markdown);
    }

    public function search($word)
    {
        $answer = false;
        try {

            $this->pssearch->bindParam(':WORD', $word, PDO::PARAM_STR);
            $this->pssearch->bindParam(':FROM', $this->from, PDO::PARAM_INT);
            $this->pssearch->bindParam(':OFFSET', $this->offset, PDO::PARAM_INT);

            if ($this->pssearch->execute()) {
                $answer = $this->pssearch->fetchAll(PDO::FETCH_ASSOC);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        return $answer;
    }

    public static function getHtmlFromUrl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT_SEARCH);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    private function saveWebsite(string $url, string $html, string $markdown)
    {
        $answer = false;
        try {
            $this->psSaveWebsite->bindParam(':URL', $url, PDO::PARAM_STR);
            $this->psSaveWebsite->bindParam(':HTML', $html, PDO::PARAM_STR);
            $this->psSaveWebsite->bindParam(':MARKDOWN', $markdown, PDO::PARAM_STR);
            $this->psSaveWebsite->bindParam(':FROM', $this->from, PDO::PARAM_INT);
            $this->psSaveWebsite->bindParam(':OFFSET', $this->offset, PDO::PARAM_INT);

            if ($this->psSaveWebsite->execute()) {
                $answer = $this->psSaveWebsite->fetchAll(PDO::FETCH_ASSOC);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        return $answer;
    }
}