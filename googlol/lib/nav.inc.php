<?php
$pagename = basename($_SERVER['PHP_SELF']);
$indexClass = $pagename == "index.php" ? 'class="active"' : '';
$markdownClass = $pagename == "addsite.php" ? 'class="active"' : '';
?>
<ul>
    <li><a <?= $indexClass ?> href="index.php">Search Word</a></li>
    <li><a <?= $markdownClass ?> href="addsite.php">Add Site</a></li>
</ul>