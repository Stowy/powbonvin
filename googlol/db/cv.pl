#!/usr/bin/perl

$cpt = 1;
  while (<>) {
    chomp;
    s/\r//g;
    s/\x{FEFF}//g;
    s/\n//g;
    s/'/\\'/g;
    s/""/''/g;
    s/"/'/g;
    print "\n($cpt,$_),";
    $cpt++;
}
