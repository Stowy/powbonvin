<?php

use League\HTMLToMarkdown\HtmlConverter;

require "./lib/constantes.inc.php";
require "./lib/db.inc.php";

const PRETTY_ID = [
    "idfrancais" => "ID Français",
    "mot" => "Mot",
    "definition" => "Définition"
];

function getHtmlFromUrl($url)
{
    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

/**
 * Converti le tableau de notes PHP en tableau HTML
 * @param mixed $array 
 * @return string 
 */
function wordsToHtmlTable($array)
{
    $html = "";
    if (!empty($array)) {

        $html .= "\n  <table>";

        // Affichage de l'entete
        $html .= "\n    <tr class=\"table-header\" >";
        foreach ($array[0] as $key => $value) {
            // We don't want to print the id
            if ($key === "idfrancais") {
                continue;
            }

            // Print the key in a pretty way
            $prettyKey = $key;
            if (array_key_exists($key, PRETTY_ID)) {
                $prettyKey = PRETTY_ID[$key];
            }

            $html .= "\n      <th>$prettyKey</th>";
        }
        $html .= "\n    </tr>\n";

        // Chaque ligne
        foreach ($array as $line) {
            $html .= "\n    <tr>";
            // Contient un  tableau
            foreach ($line as $key => $value) {
                if ($key === "idfrancais") {
                    continue;
                }

                $html .= "\n      <td>$value</td>\n";
            }
            $html .= "\n    </tr>\n";
        }
        $html .= "\n  </table>\n";
    }
    return $html;
}