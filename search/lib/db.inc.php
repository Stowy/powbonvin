<?php

/**
 * Connecteur de la base de données du .
 * Le script meurt (die) si la connexion n'est pas possible.
 * @staticvar PDO $dbc
 * @return \PDO
 */
function dbdictionnaire()
{
    static $dbc = null;

    // Première visite de la fonction
    if ($dbc == null) {
        // Essaie le code ci-dessous
        try {
            $dbc = new PDO('mysql:host=' . HOST . ';dbname=' . DBNAME, DBUSER, DBPWD, array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_PERSISTENT => true
            ));
        }
        // Si une exception est arrivée
        catch (Exception $e) {
            echo 'Erreur : ' . $e->getMessage() . '<br />';
            echo 'N° : ' . $e->getCode();
            // Quitte le script et meurt
            die('Could not connect to MySQL');
        }
    }
    // Pas d'erreur, retourne un connecteur
    return $dbc;
}

function searchWord($search, $from = 0, $offset = 50)
{
    static $ps = null;
    $sql = 'SELECT idfrancais, mot, definition';
    $sql .= ' FROM ' . DBNAME . '.' . TABLENAME;
    $sql .= ' WHERE MATCH (mot, definition) AGAINST (:WORD)';
    $sql .= ' LIMIT :FROM,:OFFSET';

    if ($ps == null) {
        $ps = dbdictionnaire()->prepare($sql);
    }
    $answer = false;
    try {
        $ps->bindParam(':WORD', $search, PDO::PARAM_STR);
        $ps->bindParam(':FROM', $from, PDO::PARAM_INT);
        $ps->bindParam(':OFFSET', $offset, PDO::PARAM_INT);

        if ($ps->execute())
            $answer = $ps->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    return $answer;
}

/**
 * Retourne les données d'une note en fonction de son idfrancais
 * @param mixed $idfrancais 
 * @return false|array 
 */
function readWord($idfrancais)
{
    static $ps = null;
    $sql = 'SELECT idfrancais, mot, definition';
    $sql .= ' FROM ' . DBNAME . '.' . TABLENAME;
    $sql .= ' WHERE idfrancais = :IDFRANCAIS';

    if ($ps == null) {
        $ps = dbdictionnaire()->prepare($sql);
    }
    $answer = false;
    try {
        $ps->bindParam(':IDFRANCAIS', $idfrancais, PDO::PARAM_INT);

        if ($ps->execute())
            $answer = $ps->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    return $answer;
}

/**
 * Lit les 50 premiers mots par défaut.
 * @staticvar pdo prepare statement $ps
 * @param int $from A partir de la note $from (0 par défaut)
 * @param int Jusqu'à la note $offset (50 par défaut) 
 * @return false|array 
 */
function readWords($from = 0, $offset = 50)
{
    static $ps = null;
    $sql = 'SELECT idfrancais, mot, definition';
    $sql .= ' FROM ' . DBNAME . '.' . TABLENAME;
    $sql .= ' LIMIT :FROM,:OFFSET;';

    if ($ps == null) {
        $ps = dbdictionnaire()->prepare($sql);
    }

    $answer = false;
    try {
        $ps->bindParam(':FROM', $from, PDO::PARAM_INT);
        $ps->bindParam(':OFFSET', $offset, PDO::PARAM_INT);

        if ($ps->execute())
            $answer = $ps->fetchAll(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    return $answer;
}