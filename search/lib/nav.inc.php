<?php
$pagename = basename($_SERVER['PHP_SELF']);
$indexClass = $pagename == "index.php" ? 'class="active"' : '';
$markdownClass = $pagename == "markdown.php" ? 'class="active"' : '';
?>
<ul>
    <li><a <?= $indexClass ?> href="index.php">Search Word</a></li>
    <li><a <?= $markdownClass ?> href="markdown.php">Html to Markdown</a></li>
</ul>