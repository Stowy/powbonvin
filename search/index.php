<!DOCTYPE html>
<?php
require_once './lib/functions.inc.php';
require_once './lib/db.inc.php';

$search = filter_input(INPUT_POST, "recherche");

if (empty($search)) {
    // Load the first batch of words
    if (!isset($loadedWords)) {
        $loadedWords = readWords();
    }
} else {
    // Search the word
    $loadedWords = searchWord($search);
}
?>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Recherche dico</title>
    <link rel="stylesheet" type="text/css" href="./lib/crud.css">
</head>

<body>
    <nav>
        <?php include "./lib/nav.inc.php"; ?>
    </nav>
    <h1>Search word</h1>
    <form action="#" method="post">
        <label for="recherche">Recherchez un mot :</label>
        <input type="text" name="recherche" id="recherche" class="searchBox" value="<?= $search ?>">
        <input type="submit" value="Chercher" class="searchButton">
    </form>
    <?php
    echo wordsToHtmlTable($loadedWords);
    ?>
</body>

</html>