<!DOCTYPE html>
<?php
require './vendor/autoload.php';

use League\HTMLToMarkdown\HtmlConverter;

require_once './lib/functions.inc.php';
require_once './lib/db.inc.php';

$urlInput = filter_input(INPUT_POST, "urlInput");

$markdown = "";

if (!empty($urlInput)) {
    $html = getHtmlFromUrl($urlInput);
    $converter = new HtmlConverter([
        "strip_tags" => true,
        "remove_nodes" => "php a style script footer noscript head nav",
        "hard_break" => true,
        "use_autolinks" => true,
        "header_style" => "atx",
    ]);

    $markdown = $converter->convert($html);
}
?>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Html to Markdown</title>
    <link rel="stylesheet" type="text/css" href="./lib/crud.css">
</head>

<body>
    <nav>
        <?php include "./lib/nav.inc.php"; ?>
    </nav>
    <h1>Html to Markdown</h1>
    <form action="#" method="post">
        <label for="urlInput">Entrez une url</label>
        <input type="text" name="urlInput" id="urlInput" class="searchBox" value="<?= $urlInput ?>">
        <input type="submit" value="To Markdown" class="searchButton">
    </form>
    <pre>
        <code>
        <?= $markdown ?>
        </code>
    </pre>
</body>

</html>